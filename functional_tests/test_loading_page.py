from .base import FunctionalTest
from selenium import webdriver

class NewVisitorTest(FunctionalTest):

    def test_can_load_web_page(self):
        #Edna opens her browser and navigates to the website
        self.browser.get(self.live_server_url)

        #She notices it says Mark Bustin in the title
        self.assertIn('Mark Bustin', self.browser.title)

    def test_has_contact_info(self):
        self.browser.get(self.live_server_url)
        contact_info_table = self.browser.find_element_by_id('id_contact_info_table').text

        self.assertIn('mark@bustin.io', contact_info_table)
    
    def test_layout_and_styling(self):
        # Edith goes to the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # She notices the input box is nicely centered
        contact_info_table = self.browser.find_element_by_id('id_contact_info_table')
        self.assertAlmostEqual(
            contact_info_table.location['x'] + contact_info_table.size['width'] / 2,
            512,
            delta=10
        )